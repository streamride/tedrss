package net.tedrss;

import android.content.Context;
import android.util.Log;

import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.log.CustomLogger;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class TedClient {


    private final Context mContext;
    private static TedClient self;
    private JobManager jobManager;

    public TedClient(Context context) {
        self = this;
        mContext = context;
        configureJobManager();
    }


    public static TedClient getInstance() {
        return self;
    }


    public JobManager getJobManager() {
        return jobManager;
    }

    private void configureJobManager() {
        Configuration configuration = new Configuration.Builder(mContext)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";

                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minute
                .build();
        jobManager = new JobManager(mContext, configuration);
    }
}


