package net.tedrss.utils;

import android.util.Log;

import net.tedrss.models.TedTalk;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class RssParser {


    private TedTalk tedTalk;

    public List<TedTalk> parse(InputStream open) throws IOException, XmlPullParserException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(false);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(open, null);

        boolean insideItem = false;
        List<TedTalk> tedTalkList = new ArrayList<>();

        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {

            if (eventType == XmlPullParser.START_TAG) {
                if (xpp.getName().equalsIgnoreCase("item")) {
                    insideItem = true;
                    tedTalk = new TedTalk();
                } else if (xpp.getName().equalsIgnoreCase("title")) {
                    if (insideItem)
                        tedTalk.title = xpp.nextText();
                } else if (xpp.getName().equalsIgnoreCase("description")) {
                    if (insideItem)
                        tedTalk.description = xpp.nextText();
                } else if (xpp.getName().equalsIgnoreCase("media:content")) {
                    if (insideItem)
                        tedTalk.url = xpp.getAttributeValue(null, "url");
                } else if (xpp.getName().equalsIgnoreCase("media:thumbnail")) {
                    if(insideItem) {
                        tedTalk.thumbNailUrl = xpp.getAttributeValue(null, "url");
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                insideItem = false;
                if (tedTalk != null) {
                    tedTalkList.add(tedTalk);
                    tedTalk = null;
                }
            }

            eventType = xpp.next();
        }
        return tedTalkList;
    }
}
