package net.tedrss.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.tedrss.R;
import net.tedrss.models.TedTalk;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class TedTalksAdapter extends RecyclerView.Adapter<TedTalksAdapter.ViewHolder> {


    private final Context mContext;
    private final View.OnClickListener mOnClickListener;
    private List<TedTalk> mTedTalkList;

    public TedTalksAdapter(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        mOnClickListener = onClickListener;
    }

    public void addData(List<TedTalk> tedTalkList) {
        if (tedTalkList == null)
            return;
        if (mTedTalkList == null) {
            mTedTalkList = new ArrayList<>();
        }

        mTedTalkList.addAll(tedTalkList);
        notifyDataSetChanged();
    }

    public TedTalk getItem(int position) {
        if (mTedTalkList != null) {
            return mTedTalkList.get(position);
        }
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ted, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mTedTalkList != null) {
            TedTalk tedTalk = mTedTalkList.get(position);
            holder.descriptionTv.setText(tedTalk.title);
            if (!TextUtils.isEmpty(tedTalk.thumbNailUrl))
                Picasso.with(mContext).load(tedTalk.thumbNailUrl).into(holder.tedThumbImg);
        }
    }

    @Override
    public int getItemCount() {
        if (mTedTalkList == null)
            return 0;
        return mTedTalkList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView descriptionTv;
        public ImageView tedThumbImg;

        public ViewHolder(View itemView) {
            super(itemView);
            tedThumbImg = (ImageView) itemView.findViewById(R.id.ted_thumb_img);
            descriptionTv = (TextView) itemView.findViewById(R.id.ted_description);
        }
    }
}
