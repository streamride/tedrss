package net.tedrss;

import android.app.Application;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class TedApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new TedClient(this);
    }
}
