package net.tedrss.actions;

import net.tedrss.models.TedTalk;

import java.util.List;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class TalksAction {

    private final List<TedTalk> mTedTalks;

    public TalksAction(List<TedTalk> tedTalkList) {
        mTedTalks = tedTalkList;
    }


    public List<TedTalk> getTedTalks() {
        return mTedTalks;
    }
}
