package net.tedrss.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import net.tedrss.R;

/**
 * Created by andreyzakharov on 04.07.15.
 */
public class VideoActivity extends Activity {

    public static final String PARAM_TED_VIDEO_URL = "param_ted_video_uri";
    private VideoView mTedVideoView;
    private String mVideoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_layout);

        mTedVideoView = (VideoView)findViewById(R.id.ted_video_view);

        if (getIntent().hasExtra(PARAM_TED_VIDEO_URL)){
            mVideoUri = getIntent().getStringExtra(PARAM_TED_VIDEO_URL);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mVideoUri != null) {

            mTedVideoView.setVideoPath(mVideoUri);
            mTedVideoView.setMediaController(new MediaController(this));
            mTedVideoView.requestFocus();
            mTedVideoView.start();
        }
    }
}
