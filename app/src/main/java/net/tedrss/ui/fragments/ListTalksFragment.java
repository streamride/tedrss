package net.tedrss.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.tedrss.R;
import net.tedrss.TedClient;
import net.tedrss.actions.TalksAction;
import net.tedrss.adapters.TedTalksAdapter;
import net.tedrss.jobs.GetTalksJob;
import net.tedrss.models.TedTalk;
import net.tedrss.ui.VideoActivity;

import de.greenrobot.event.EventBus;

/**
 * Created by andreyzakharov on 03.07.15.
 */
public class ListTalksFragment extends Fragment {


    private View mRootView;
    private RecyclerView mTedRecyclerView;
    private TedTalksAdapter mTedAdapter;
    private int mCurrentPage = 1;

    private View.OnClickListener tedItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = mTedRecyclerView.getChildAdapterPosition(v);
            TedTalk tedTalk = mTedAdapter.getItem(index);
            if (tedTalk != null) {
                Intent intent = new Intent(getActivity(), VideoActivity.class);
                intent.putExtra(VideoActivity.PARAM_TED_VIDEO_URL, tedTalk.url);
                getActivity().startActivity(intent);
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mTedAdapter = new TedTalksAdapter(getActivity(), tedItemClickListener);
        loadTedTalks();
    }

    private void loadTedTalks() {
        TedClient.getInstance().getJobManager().addJob(new GetTalksJob(mCurrentPage));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list_talks, container, false);

        mTedRecyclerView =(RecyclerView) mRootView.findViewById(R.id.ted_list_rv);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mTedRecyclerView.setLayoutManager(linearLayoutManager);

        mTedRecyclerView.setAdapter(mTedAdapter);

        mTedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibilePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (mTedAdapter.getItemCount()-1 == lastVisibilePosition) {
                    mCurrentPage++;
                    loadTedTalks();
                }
            }
        });
        return mRootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(TalksAction talksAction) {
        mTedAdapter.addData(talksAction.getTedTalks());
    }
}
