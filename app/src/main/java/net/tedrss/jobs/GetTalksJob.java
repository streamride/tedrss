package net.tedrss.jobs;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.tedrss.C;
import net.tedrss.actions.TalksAction;
import net.tedrss.models.TedTalk;
import net.tedrss.utils.RssParser;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * Created by andreyzakharov on 03.07.15.
 */
public class GetTalksJob extends Job {


    private final int mPage;

    public GetTalksJob(int page) {
        super(new Params(1).requireNetwork());
        mPage = page;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(String.format(C.TALKS_URL, mPage)).get().build();
        Response response = okHttpClient.newCall(request).execute();
        RssParser rssParser = new RssParser();
        List<TedTalk> tedTalkList = rssParser.parse(response.body().byteStream());
        EventBus.getDefault().post(new TalksAction(tedTalkList));
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }
}
